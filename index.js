fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {
	let titles = json.map(item => item.title);
	console.log(titles);
});


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
})
.then((response) => response.json())
.then((json) => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Created To Do List Item",
		completed: false,
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Updated To Do List Item",
		dateCompleted: "Pending",
		description: "To update my to do list with a different data structure",
		status: "pending"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		title: "delectus aut auters",
		status: "Complete",
		dateCompleted: "07/09/2021",
		userId: "1"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


